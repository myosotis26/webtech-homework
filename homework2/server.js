const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
});

app.put('/products/:id', (req, res) => {
    var succes = 0;
	var id = parseInt(req.params.id);
	var updatedProduct = req.body;
	
	for(var i=0; i<products.length; i++) {
	    var object = products[i];
	    if(object.id===id) {
	        products[i]=updatedProduct;
	        succes = 1;
	        
	        console.log("--->Update Successfully, products: \n" + JSON.stringify(products, null, 4))
	        
	        // return
		    res.end("Update Successfully! \n" + JSON.stringify(updatedProduct, null, 4));
	    }
	}
	
	if(succes==0){
	    res.status(500).send('Error!');
	}
});

app.delete('/products/:name', (req, res) => {
    var name = req.params.name;
    var succes = 0;
    
    for(var i=0; i<products.length; i++) {
        var object = products[i];
        var nameWithoutSpaces = object.productName.replace(/ /g,'');
        if(nameWithoutSpaces===name) {
            var deleteProduct = products[i];
            delete products[i];
            var succes = 1;
            console.log("--->After deletion, products list:\n" + JSON.stringify(products, null, 4) );
            res.end( "Deleted product: \n" + JSON.stringify(deleteProduct, null, 4));
        }
    }
    
    if(succes==0){
	    res.status(500).send('Error!');
	}
});

app.delete('/delete', (req, res) => {
    var name = req.body.productName;
    var succes = 0;
    
    for(var i=0; i<products.length; i++) {
        var object = products[i];
        if(object.productName===name) {
            var deleteProduct = products[i];
            delete products[i];
            succes = 1;
            console.log("--->After deletion, products list:\n" + JSON.stringify(products, null, 4) );
            res.end( "Deleted product: \n" + JSON.stringify(deleteProduct, null, 4));
        }
    }
    
    if(succes==0){
	    res.status(500).send('Error!');
	}
    
});

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});